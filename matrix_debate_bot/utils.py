import json


DATA_FILE = "data.json"

def get_state(filename=DATA_FILE):
    try: 
        with open(filename, 'r') as f:
            state = json.loads(f.read())
    except FileNotFoundError as e:
        print(f"No data found at {filename}, continuing with no data")
        state = {}
    return state

def set_state(state, filename=DATA_FILE):
    with open(filename, 'w') as f:
        f.write(json.dumps(state))

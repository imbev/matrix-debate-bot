import asyncio
import simplematrixbotlib as botlib
import sys; sys.path.append("..")
from creds import creds
from .utils import set_state, get_state


bot = botlib.Bot(creds)
PREFIX = '!debate'
DATA_FILE = "data.json"


@bot.listener.on_message_event
async def start(room, message):
    match = botlib.MessageMatch(room, message, bot, PREFIX)

    if match.is_not_from_this_bot() and match.prefix() and match.command(
            "start"):
        state = get_state()
        state.update({room.room_id:True})
        set_state(state)
        await bot.api.send_text_message(room.room_id, "Debate Started!")
        
        async def turn():
            while get_state().get(room.room_id):
                await bot.api.send_text_message(room.room_id, "Turn over. Next side has 2 minutes.")
                await asyncio.sleep(2*60)
        asyncio.ensure_future(turn())

@bot.listener.on_message_event
async def end(room, message):
    match = botlib.MessageMatch(room, message, bot, PREFIX)

    if match.is_not_from_this_bot() and match.prefix() and match.command(
            "end"):
        state = get_state()
        state.update({room.room_id:False})
        set_state(state)
        
        await bot.api.send_text_message(room.room_id, "The Debate has ended.")

def main():
    bot.run()

if __name__ == '__main__':
    main()

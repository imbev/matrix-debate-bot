# matrix-debate-bot

A bot to moderate debate in Matrix rooms.

## Setup

Requires Python ^3.6

```sh
python3 -m pip install poetry
git clone https://gitlab.com/imbev/matrix-debate-bot.git
cd matrix-debate-bot
python3 -m poetry install
```

## Usage

### Credentials

Create a python file with a filename of `creds.py` in the same directory as the 
project. In this file, create an instance of the Creds class as specified in
https://simple-matrix-bot-lib.readthedocs.io/en/latest/manual.html#usage-of-creds-class

example:

```python
# ./creds.py

import simplematrixbotlib as botlib

creds = botlib.Creds(
    homeserver="https://example.org",
    username="username",
    password="password",
    )
```

### Running

```sh
python3 -m poetry run bot
```

### Interacting

- Starting a debate

`!debate start`

After a 2 minute delay, the bot will send a message with "Turn over. Next side 
has 2 minutes." This will repeat until the debate has ended.

- Ending a debate

`!debate end`

This will end the debate, causing the turns to stop repeating.

## Misc

- Free and Open Source, Licensed under the GPL-3.0-only license.
